
import 'package:restaurante/datasources/create_db.dart';
import 'package:restaurante/models/cliente.dart';
import 'package:sqflite/sqflite.dart';
import 'package:restaurante/utils/utils.dart';

class ClienteDataSource{

  DatabaseRestaurante db;

  ClienteDataSource(){
    db = DatabaseRestaurante();
  }

  Future<Cliente> saveCliente(Cliente cliente) async{

    Database dbCliente = await db.get_db;

    cliente.id = await dbCliente.insert(clienteTable, cliente.toMap());
    print("Salvou a cliente ${cliente.id}");
    return cliente;
  }

  Future<Cliente> getCliente(int id) async {
    Database dbCliente = await db.get_db;
//    dbCliente.rawQuery(sql);
    //Faz a pesquisa que retorna uma lista de mapas
    List<Map> maps = await dbCliente.query(
        clienteTable,
        columns: [idColumn, nameColumn, emailColumn, phoneColumn, imgColumn],
        where: "$idColumn = ?",
        whereArgs: [id]
    );

    //Se retornou algum cliente
    if(maps.length > 0)
      return Cliente.fromMap(maps.first);
    //Se não houve retorno
    return null;

  }

  Future<int> deleteCliente(int id) async {
    Database dbCliente = await db.get_db;

    return await dbCliente.delete(
        clienteTable,
        where: "$idColumn = ?",
        whereArgs: [id]
    );
  }

  Future<int> updateCliente(Cliente cliente) async {
    Database dbCliente = await db.get_db;

    return await dbCliente.update(
        clienteTable,
        cliente.toMap(),
        where: "$idColumn = ?",
        whereArgs: [cliente.id]
    );
  }

  Future<List<Cliente>> getAllClientes() async {
    Database dbCliente = await db.get_db;

    //Pega todas as linhas do banco
    List listMap = await dbCliente.rawQuery("SELECT * FROM $clienteTable");

    List<Cliente> listCliente = List();

    listMap.forEach((item) {
      listCliente.add(Cliente.fromMap(item));
    });

    print("Procurou todas os clientes");

    return listCliente;
  }

  Future<void> relacionaMesaCliente(int idMesa, int idCliente) async {
    Database dbMesa = await db.get_db;

    await dbMesa.execute("UPDATE $mesaTable SET $idClienteMesa = $idCliente WHERE $idColumn = $idMesa");
  }

  Future<int> getNumber() async {
    Database dbCliente = await db.get_db;

    return Sqflite.firstIntValue(await dbCliente.rawQuery("SELECT COUNT(*) FROM $clienteTable"));
  }

  Future<void> close() async {
    Database dbCliente = await db.get_db;

    return await dbCliente.close();
  }

}