import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:restaurante/utils/utils.dart';

class DatabaseRestaurante{

  static final DatabaseRestaurante _instance = DatabaseRestaurante.internal();

  factory DatabaseRestaurante() => _instance;

  DatabaseRestaurante.internal();

  Database _db;

  Future<Database> get get_db async {
    //Caso o banco não tenha sido criado
    if(_db == null)
      _db = await initDB();
    return _db;
  }

  //Inicia o banco com SQL
  Future<Database> initDB() async {
    var databasesPath = await getDatabasesPath();

    final path = join(databasesPath, "restaurante7.db");

    return await openDatabase(
        path,
        version: 1,
        onCreate: (Database db, int newerVersion) async {
          await db.execute("CREATE TABLE $mesaTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$idClienteMesa INTEGER, "
              "$ocupColumn INTEGER,"
              "FOREIGN KEY ($idClienteMesa) REFERENCES $clienteTable ($idColumn)"
              "ON DELETE NO ACTION ON UPDATE NO ACTION)");

          await db.execute("CREATE TABLE $clienteTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$nameColumn TEXT, "
              "$emailColumn TEXT, "
              "$phoneColumn TEXT, "
              "$imgColumn TEXT)");

          await db.execute("CREATE TABLE $itemTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$nomeColumn TEXT, "
              "$precoColumn REAL,"
              "$imgColumn TEXT)");

          await db.execute("CREATE TABLE $pedidoTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$dataColumn TEXT, "
              "$idClientePedido INTEGER, "
              "$quantidadeColumn INTEGER, "
              "$idItemPedido INTEGER, "
              "FOREIGN KEY ($idClientePedido) REFERENCES $clienteTable ($idColumn), "
              "FOREIGN KEY ($idItemPedido) REFERENCES $itemTable ($idColumn))");
        }
    );
  }
}