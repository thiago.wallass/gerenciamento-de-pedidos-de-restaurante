import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:restaurante/datasources/create_db.dart';
import 'package:restaurante/models/item.dart';
import 'package:sqflite/sqflite.dart';
import 'package:restaurante/utils/utils.dart';

class ItemDataSource{

  DatabaseRestaurante db;

  ItemDataSource(){
    db = DatabaseRestaurante();
  }

  Future<Item> saveItem(Item item) async{

    Database dbItem = await db.get_db;

    item.id = await dbItem.insert(itemTable, item.toMap());
    print("Salvou a item ${item.id}");
    return item;
  }

  Future<Item> getItem(int id) async {
    Database dbItem = await db.get_db;
//    dbItem.rawQuery(sql);
    //Faz a pesquisa que retorna uma lista de mapas
    List<Map> maps = await dbItem.query(
        itemTable,
        columns: [idColumn, nomeColumn, precoColumn
//          , quantidadeColumn
        ],
        where: "$idColumn = ?",
        whereArgs: [id]
    );

    //Se retornou algum item
    if(maps.length > 0)
      return Item.fromMap(maps.first);
    //Se não houve retorno
    return null;

  }

  Future<int> deleteItem(int id) async {
    Database dbItem = await db.get_db;

    return await dbItem.delete(
        itemTable,
        where: "$idColumn = ?",
        whereArgs: [id]
    );
  }

  Future<int> updateItem(Item item) async {
    Database dbItem = await db.get_db;

    return await dbItem.update(
        itemTable,
        item.toMap(),
        where: "$idColumn = ?",
        whereArgs: [item.id]
    );
  }

  Future<List<Item>> getAllItems() async {
//    Database dbItem = await db.get_db;
//
//    //Pega todas as linhas do banco
//    List listMap = await dbItem.rawQuery("SELECT * FROM $itemTable");
//
//    List<Item> listItem = List();
//
//    listMap.forEach((item) {
//      listItem.add(Item.fromMap(item));
//    });
//
//    print("Procurou todos os items");
//    print(listItem);

//    var list = List<String>.from(snapshot.data['']);

//    DocumentSnapshot querySnapshot = await Firestore
//        .instance
//        .collection('itens')
//        .document('D0lC7Xn7J0ur6jj4QTHK')
//        .get();
//
////    List<Item> listItem = List();
//
//    if (querySnapshot.exists) {
//      // Create a new List<String> from List<dynamic>
//      var list = List<String>.from(querySnapshot.data['itens']);
//
//      list.forEach((item) {
////        listItem.add(Item.fromMap(item));
//        print(item);
//      });
//    }

//    var snapshots = Firestore.instance.collection('itens').document().snapshots();
//    snapshots.map((data) => Item.fromMap(data).toList();
//    print(snapshots);

    List<Item> listItem = List();

    Firestore.instance
        .collection('itens')
        .snapshots()
        .listen((data) =>
        data.documents.forEach((item) => listItem.add(Item.fromMap(item.data))));
    print(listItem);

    return listItem;
  }

  Future<int> getNumber() async {
    Database dbItem = await db.get_db;

    return Sqflite.firstIntValue(await dbItem.rawQuery("SELECT COUNT(*) FROM $itemTable"));
  }

  Future<void> close() async {
    Database dbItem = await db.get_db;

    return await dbItem.close();
  }
  
}