import 'package:path/path.dart';
import 'package:restaurante/datasources/create_db.dart';
import 'package:restaurante/models/mesa.dart';
import 'package:sqflite/sqflite.dart';
import 'package:restaurante/utils/utils.dart';


class MesaDataSource{

  DatabaseRestaurante db;

  MesaDataSource(){
    db = DatabaseRestaurante();
  }

  Future<Mesa> saveMesa(Mesa mesa) async{

    Database dbMesa = await db.get_db;

    mesa.id = await dbMesa.insert(mesaTable, mesa.toMap());
    print("Salvou a mesa ${mesa.id}");
    return mesa;
  }

//  Future<Mesa> getMesa(int id) async {
//    Database dbMesa = await db.get_db;
////    dbMesa.rawQuery(sql);
//    //Faz a pesquisa que retorna uma lista de mapas
//    List<Map> maps = await dbMesa.query(
//        mesaTable,
//        columns: [idColumn, nameColumn, emailColumn, phoneColumn, imgColumn],
//        where: "$idColumn = ?",
//        whereArgs: [id]
//    );
//
//    //Se retornou algum contato
//    if(maps.length > 0)
//      return Mesa.fromMap(maps.first);
//    //Se não houve retorno
//    return null;
//
//  }

  Future<int> deleteMesa(int id) async {
    Database dbMesa = await db.get_db;

    return await dbMesa.delete(
        mesaTable,
        where: "$idColumn = ?",
        whereArgs: [id]
    );
  }

  Future<int> updateMesa(Mesa mesa) async {
    Database dbMesa = await db.get_db;

    return await dbMesa.update(
        mesaTable,
        mesa.toMap(),
        where: "$idColumn = ?",
        whereArgs: [mesa.id]
    );
  }

  Future<List<Mesa>> getAllMesas() async {
    Database dbMesa = await db.get_db;

    //Pega todas as linhas do banco
    List listMap = await dbMesa.rawQuery("SELECT * FROM $mesaTable");

    List<Mesa> listMesa = List();

    listMap.forEach((item) {
      listMesa.add(Mesa.fromMap(item));
    });

    print("Procurou todas as mesas");

    return listMesa;
  }

  Future<int> getNumber() async {
    Database dbMesa = await db.get_db;

    return Sqflite.firstIntValue(await dbMesa.rawQuery("SELECT COUNT(*) FROM $mesaTable"));
  }

  Future<void> close() async {
    Database dbMesa = await db.get_db;

    return await dbMesa.close();
  }

}