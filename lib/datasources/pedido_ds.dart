import 'package:restaurante/datasources/create_db.dart';
import 'package:restaurante/models/pedido.dart';
import 'package:sqflite/sqflite.dart';
import 'package:restaurante/utils/utils.dart';

class PedidoDataSource{

  DatabaseRestaurante db;

  PedidoDataSource(){
    db = DatabaseRestaurante();
  }

  Future<Pedido> savePedido(Pedido pedido) async{

    Database dbPedido = await db.get_db;

    pedido.id = await dbPedido.insert(pedidoTable, pedido.toMap());
    print("Salvou a pedido ${pedido.id}");
    return pedido;
  }

  Future<Pedido> getPedido(int id) async {
    Database dbPedido = await db.get_db;
//    dbPedido.rawQuery(sql);
    //Faz a pesquisa que retorna uma lista de mapas
    List<Map> maps = await dbPedido.query(
        pedidoTable,
        columns: [idColumn, idItemPedido, idClientePedido, dataColumn],
        where: "$idColumn = ?",
        whereArgs: [id]
    );

    //Se retornou algum pedido
    if(maps.length > 0)
      return Pedido.fromMap(maps.first);
    //Se não houve retorno
    return null;

  }

  Future<int> deletePedido(int id) async {
    Database dbPedido = await db.get_db;

    return await dbPedido.delete(
        pedidoTable,
        where: "$idColumn = ?",
        whereArgs: [id]
    );
  }

  Future<int> updatePedido(Pedido pedido) async {
    Database dbPedido = await db.get_db;

    return await dbPedido.update(
        pedidoTable,
        pedido.toMap(),
        where: "$idColumn = ?",
        whereArgs: [pedido.id]
    );
  }

  Future<List<Pedido>> getAllPedidos() async {
    Database dbPedido = await db.get_db;

    //Pega todas as linhas do banco
    List listMap = await dbPedido.rawQuery("SELECT * FROM $pedidoTable");

    return convertListToPedido(listMap);
  }

  Future<List<Pedido>> getPedidosCliente(int idCliente) async {

    Database dbPedido = await db.get_db;

    List listMap = await dbPedido.rawQuery("SELECT * FROM $pedidoTable WHERE $idClientePedido = $idCliente");

    return convertListToPedido(listMap);

  }

  List<Pedido> convertListToPedido(List listMap){

    List<Pedido> listPedido = List();

    listMap.forEach((pedido) {
      listPedido.add(Pedido.fromMap(pedido));
    });

    return listPedido;
  }

  Future<int> getNumber() async {
    Database dbPedido = await db.get_db;

    return Sqflite.firstIntValue(await dbPedido.rawQuery("SELECT COUNT(*) FROM $pedidoTable"));
  }

  Future<void> close() async {
    Database dbPedido = await db.get_db;

    return await dbPedido.close();
  }

}