import 'package:flutter/material.dart';
import 'package:restaurante/screens/home_page_sc.dart';

void main(){
  runApp(MaterialApp(
    home: HomePage(),
    theme: ThemeData(
      primarySwatch: Colors.red,
    ),
    debugShowCheckedModeBanner: false,
  ));
}
