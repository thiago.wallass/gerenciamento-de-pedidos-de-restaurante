import 'package:restaurante/utils/utils.dart';

class Cliente {

  int id;
  String name;
  String email;
  String phone;
  String img;

  Cliente();

  Cliente.fromMap(Map map){
    id = map[idColumn];
    name = map[nameColumn];
    email = map[emailColumn];
    phone = map[phoneColumn];
    img = map[imgColumn];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      nameColumn: name,
      emailColumn: email,
      phoneColumn: phone,
      imgColumn: img
    };

    if (id != null)
      map[idColumn] = id;

    return map;
  }

  @override
  String toString() {
    return "Cliente(id: $id, name: $name, email: $email, phone: $phone, img: $img)";
  }

}