import 'package:restaurante/utils/utils.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';

class Item{

  int id;
  String nome;
  String descricao;
  num preco;
  String img;
//  DocumentReference reference;

  Item();

  Item.fromMap(Map map, ){
    id = map[idColumn];
    nome = map[nomeColumn];
    descricao = map[descricaoColumn];
    preco = map[precoColumn];
    img = map[imgColumn];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      nomeColumn: nome,
      precoColumn: preco,
      imgColumn: img
    };

    if (id != null)
      map[idColumn] = id;

    return map;
  }

//  Item.fromSnapshot(DocumentSnapshot snapshot)
//      : this.fromMap(snapshot.data, reference: snapshot.reference);

}