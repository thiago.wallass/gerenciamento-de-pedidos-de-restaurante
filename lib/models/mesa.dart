import 'package:restaurante/models/cliente.dart';
import 'package:restaurante/utils/utils.dart';

class Mesa {

  int id;
  int idCliente;
  bool ocup;
  String name;

  Mesa(){
    ocup = false;
    idCliente = null;
  }

  Mesa.fromMap(Map map){

    id = map[idColumn];

    if(map[ocupColumn] == 0)
      ocup = false;
    else
      ocup = true;

    if(map.containsKey(idClienteMesa) && map[idClienteMesa] != null)
      idCliente = map[idClienteMesa];

  }

  Map<String, dynamic> toMap(){

    Map<String, dynamic> map = Map();

    if(id != null)
      map[idColumn] = id;

    if(idCliente != null)
      map[idClienteMesa] = idCliente;

    if(ocup)
      map[ocupColumn] = 1;
    else
      map[ocupColumn] = 0;

    return map;
  }

  void addCliente(Cliente cliente){

    idCliente = cliente.id;
    ocup = true;
  }

  @override
  String toString() {
    return "Mesa(id: $id, name: $name, idCliente: $idCliente, ocupado: $ocup)";
  }


}