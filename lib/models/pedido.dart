import 'package:restaurante/utils/utils.dart';

class Pedido{

  int id;
  int idItem;
  int idCliente;
  String data;

  Pedido();

  Pedido.fromIds(this.idItem, this.idCliente, this.data);

  Pedido.fromMap(Map map){
    id = map[idColumn];
    idItem = map[idItemPedido];
    idCliente = map[idClientePedido];
    data = map[dataColumn];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      idItemPedido: idItem,
      idClientePedido: idCliente,
      dataColumn: data
    };

    if (id != null)
      map[idColumn] = id;

    return map;
  }

  @override
  String toString(){
    return "Pedido( id: $id, idCliente: $idCliente, idItem: $idItem, data: $data)";
  }
}