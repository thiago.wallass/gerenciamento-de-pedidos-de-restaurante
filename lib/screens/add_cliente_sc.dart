import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:restaurante/models/cliente.dart';

class AddClienteScreen extends StatefulWidget {

  final Cliente cliente;

  AddClienteScreen({this.cliente});

  @override
  _AddClienteScreenState createState() => _AddClienteScreenState();
}

class _AddClienteScreenState extends State<AddClienteScreen> {

  //Flag de contato editado
  bool _userEdited = false;

  //Cliente to be edited
  Cliente _editCliente;

  //TextControllers
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();

  //FocusNode
  final _nameFocus = FocusNode();

  @override
  void initState() {
    super.initState();

    //Acessa classe ClientePage e checa se foi passado um contato por parâmetro
    if (widget.cliente == null) {
      _editCliente = new Cliente();
    } else {
      //Duplica o contato, criando uma novo contato a partir do passado como parâmetro
      //Pois caso o usuário cancele a operação, o usuário não seja editado
      _editCliente = Cliente.fromMap(widget.cliente.toMap());

      //Seta as informações do contato
      _nameController.text = _editCliente.name;
      _emailController.text = _editCliente.email;
      _phoneController.text = _editCliente.phone;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          title: Text(_editCliente.name ?? 'Novo Cliente'),
          backgroundColor: Colors.red,
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            //Se o nome não está vazio
            if( _editCliente.name != null && _editCliente.name.isNotEmpty){
              //Passa o contato editado de volta para a HomePage
              Navigator.pop(context, _editCliente);
            }else{
              //Dá foco ao nome
              FocusScope.of(context).requestFocus(_nameFocus);
            }
          },
          child: Icon(Icons.save),
          backgroundColor: Colors.red,
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    ImagePicker.pickImage(source: ImageSource.camera).then((file){
                      if(file == null) return;
                      setState(() {
                        _userEdited = true;
                        _editCliente.img = file.path;
                      });
                    });
                  },
                  child: Container(
                    width: 140,
                    height: 140,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: _editCliente.img != null ?
                            FileImage(File(_editCliente.img)) :
                            AssetImage("images/person.png")
                        )
                    ),
                  ),
                ),
                TextField(
                  controller: _nameController,
                  focusNode: _nameFocus,
                  decoration: InputDecoration(labelText: 'Nome'),
                  onChanged: (text){
                    _userEdited = true;
                    setState(() {
                      _editCliente.name = text;
                    });
                  },
                ),
                TextField(
                  controller: _emailController,
                  decoration: InputDecoration(labelText: 'Email'),
                  onChanged: (text){
                    _userEdited = true;
                    _editCliente.email = text;
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
                TextField(
                  controller: _phoneController,
                  decoration: InputDecoration(labelText: 'Telefone',),
                  keyboardType: TextInputType.number,
                  onChanged: (text){
                    _userEdited = true;
                    _editCliente.phone = text;
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //Função que é chamada quando é requisitado o pop da tela
  Future<bool> _requestPop(){
    if(_userEdited){
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Descartar Alterações?"),
              content: Text("Se sair as alterações serão perdidas."),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text("Cancelar")
                ),
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: Text("Sim")
                )
              ],
            );
          }
      );
      return Future.value(false);
    } else {
      //Saída automática da tela
      return Future.value(true);
    }
  }

}