import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:restaurante/models/item.dart';

class AddItemScreen extends StatefulWidget {

  final Item item;

  AddItemScreen({this.item});

  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {

  //Flag de contato editado
  bool _userEdited = false;

  //Item to be edited
  Item _editItem;

  //flagCozinheiro
  bool precisaCozinheiro = true;

  //TextControllers
  final _descricaoController = TextEditingController();
  final _precoController = TextEditingController();

  //FocusNode
  final _descricaoFocus = FocusNode();

  @override
  void initState() {
    super.initState();

    //Acessa classe ItemPage e checa se foi passado um contato por parâmetro
    if (widget.item == null) {
      _editItem = new Item();
    } else {
      //Duplica o contato, criando uma novo contato a partir do passado como parâmetro
      //Pois caso o usuário cancele a operação, o usuário não seja editado
      _editItem = Item.fromMap(widget.item.toMap());

      //Seta as informações do contato
      _descricaoController.text = _editItem.nome;
      _precoController.text = _editItem.preco.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          title: Text(_editItem.nome ?? 'Novo Item'),
//          title: Text('Nova Sobremesa'),
          backgroundColor: Colors.red,
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            //Se o nome não está vazio
            if( _editItem.nome != null && _editItem.nome.isNotEmpty){
              //Passa o item a ser editado de volta para a HomePage
              Navigator.pop(context, _editItem);
            }else{
              //Dá foco ao nome
              FocusScope.of(context).requestFocus(_descricaoFocus);
            }
          },
          child: Icon(Icons.save),
          backgroundColor: Colors.red,
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    ImagePicker.pickImage(source: ImageSource.camera).then((file){
                      if(file == null) return;
                      setState(() {
                        _userEdited = true;
                        _editItem.img = file.path;
                      });
                    });
                  },
                  child: Container(
                    width: 140,
                    height: 140,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: _editItem.img != null ?
                            FileImage(File(_editItem.img)) :
                            AssetImage("images/default-item.jpg")
//                            AssetImage("images/sobremesa.jpg")
//                            AssetImage("images/suco.jpg")
                        )
                    ),
                  ),
                ),
                //TODO
                TextField(
//                  controller: _descricaoController,
//                  focusNode: _descricaoFocus,
                  decoration: InputDecoration(labelText: 'Nome'),
                  onChanged: (text){
//                    _userEdited = true;
//                    setState(() {
//                      _editItem.descricao = text;
//                    });
                  },
                ),
                TextField(
                  controller: _descricaoController,
                  focusNode: _descricaoFocus,
                  decoration: InputDecoration(labelText: 'Descrição'),
                  onChanged: (text){
                    _userEdited = true;
                    setState(() {
                      _editItem.nome = text;
                    });
                  },
                ),
                TextField(
                  controller: _precoController,
                  decoration: InputDecoration(labelText: 'Preço',),
                  keyboardType: TextInputType.number,
                  onChanged: (text){
                    _userEdited = true;
                    _editItem.preco = num.parse(text);
                  },
                ),
//                TextField(
//                  controller: _precoController,
//                  decoration: InputDecoration(labelText: 'Serve '),
//                  keyboardType: TextInputType.number,
//                  onChanged: (text){
//                    _userEdited = true;
//                    _editItem.preco = num.parse(text);
//                  },
//                ),

//                TextField(
//                  controller: _precoController,
//                  decoration: InputDecoration(labelText: 'Volume'),
//                  onChanged: (text){
//                    _userEdited = true;
//                    _editItem.preco = num.parse(text);
//                  },
//                ),
                SizedBox(height: 20),
                Column(
                  children: <Widget>[
                    Text('Cozinheiro', style: TextStyle(color: Colors.grey)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Switch(
                          onChanged: (a) {
                            setState(() {
                              precisaCozinheiro = !precisaCozinheiro;
                            });
                          },
                          value: precisaCozinheiro),
                       Text(
                          precisaCozinheiro ? 'Sim' : 'Não'
                       ),
                      ]
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  //Função que é chamada quando é requisitado o pop da tela
  Future<bool> _requestPop(){
    if(_userEdited){
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Descartar Alterações?"),
              content: Text("Se sair as alterações serão perdidas."),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text("Cancelar")
                ),
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: Text("Sim")
                )
              ],
            );
          }
      );
      return Future.value(false);
    } else {
      //Saída automática da tela
      return Future.value(true);
    }
  }

}