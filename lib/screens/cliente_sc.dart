import 'dart:io';
import 'package:restaurante/datasources/mesa_ds.dart';
import 'package:restaurante/models/mesa.dart';
import 'package:restaurante/screens/add_cliente_sc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:restaurante/models/cliente.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurante/datasources/cliente_ds.dart';

class ClienteScreen extends StatefulWidget {

  static Mesa mesa;

  ClienteScreen(Mesa mesa){
    ClienteScreen.mesa = mesa;
  }

  @override
  _ClienteState createState() => _ClienteState();
}

class _ClienteState extends State<ClienteScreen> {

  ClienteDataSource dsCliente = ClienteDataSource();

  List<Cliente> clientes = List();

  @override
  void initState() {
    super.initState();

    _getAllClientes();

//    dsCliente.deleteCliente(1);
//
//    Cliente c = Cliente();
//    c.name = "José Vinicius";
//    c.email = "josevinicius@gmail.com";
//    c.phone = "987415240";
//    dsCliente.saveCliente(c);

//      Cliente c = Cliente();
//      c.name = "Thiago Wallass";
//      c.email = "thiagowallass@gmail.com";
//      c.phone = "988984133";
//      dsCliente.saveCliente(c);

//    dsCliente.deleteCliente(8).then((data) => print('apagou'));

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Clientes'),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showClientePage(),
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
        itemCount: clientes.length,
        itemBuilder: (context, index) {
          return _clienteCard(context, index);
        },
        padding: EdgeInsets.all(10),
      ),
    );
  }

  Widget _clienteCard(context, index) {
    return GestureDetector(
      onTap: () => _showOptions(context, index),
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: clientes[index].img != null
                            ? FileImage(File(clientes[index].img))
                            : AssetImage("images/person.png"))),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      clientes[index].name ?? "",
                      style:
                      TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    Text(clientes[index].email ?? "", style: TextStyle(fontSize: 18)),
                    Text(clientes[index].phone ?? "", style: TextStyle(fontSize: 18)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showOptions(BuildContext context, int index){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return BottomSheet(
              onClosing: (){},
              builder: (context){
                return Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {
                              Mesa mesa = ClienteScreen.mesa;
                              MesaDataSource dsMesa = MesaDataSource();

                              if(mesa != null){

                                mesa.addCliente(clientes[index]);

                                //Atualiza no bd
                                await dsMesa.updateMesa(mesa);
                              }

                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: Text("Adicionar à mesa",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
//                      Padding(
//                        padding: EdgeInsets.all(10),
//                        child: FlatButton(
//                            onPressed: (){
//                              launch('tel:${clientes[index].phone}');
//                              Navigator.pop(context);
//                            },
//                            child: Text("Ligar",
//                              style: TextStyle(color: Colors.red, fontSize: 20),
//                            )
//                        ),
//                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: (){
                              Navigator.pop(context);
                              _showClientePage(cliente: clientes[index]);
                            },
                            child: Text("Editar",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {
                              await dsCliente.deleteCliente(clientes[index].id);
                              _getAllClientes();
                              Navigator.pop(context);
                            },
                            child: Text("Excluir",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
                    ],
                  ),
                );
              });
        }
    );
  }

  void _showClientePage({Cliente cliente}) async {
    final recCliente = await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return AddClienteScreen(cliente: cliente);
    }));
    //Se o contato foi alterado
    if(recCliente != null){

      //Se o contato alterado é um contato novo ou um editado (parâmetro)
      if(cliente != null)
        await dsCliente.updateCliente(recCliente);
      else
        await dsCliente.saveCliente(recCliente);

      _getAllClientes();
    }
  }

  //Atualiza lista de contatos
  void _getAllClientes() {
    dsCliente.getAllClientes().then((list) {
      setState(() {
        clientes = list;
      });
    });
  }

}