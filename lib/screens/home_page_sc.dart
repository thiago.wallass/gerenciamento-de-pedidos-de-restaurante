import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurante/datasources/cliente_ds.dart';
import 'package:restaurante/datasources/create_db.dart';
import 'package:restaurante/datasources/mesa_ds.dart';
import 'package:restaurante/models/cliente.dart';
import 'package:restaurante/models/mesa.dart';
import 'package:restaurante/screens/cliente_sc.dart';
import 'package:restaurante/screens/item_sc.dart';
import 'package:restaurante/screens/pedidos_sc.dart';
import 'package:restaurante/utils/custom_widgets.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<Mesa> mesas = List();
  MesaDataSource dsMesa = MesaDataSource();
  Cliente cliente = null;

  @override
  void initState() {
    super.initState();

    DatabaseRestaurante db = new DatabaseRestaurante();

    _getAllMesas();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: CustomWidgets.appBar(
        title: "Restaurante",
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.person),
            onPressed: (){
              //Chama outra tela
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => ClienteScreen(null)
              ));
            },
          ),
          IconButton(
            icon: Icon(Icons.closed_caption),
            onPressed: (){
              //Chama outra tela
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => ItemScreen()
              ));
            },
          )
        ]
      ),
      body: Container(
        child: GridView.builder(
          itemCount: mesas.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, crossAxisSpacing: 10, mainAxisSpacing: 10),
          itemBuilder: (context, index){
            return Padding(
              padding: EdgeInsets.all(10),
              child: _buildMesa(index),
            );
          }
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          _createNewMesa();
        }
      ),
    );
  }

  Widget _buildMesa(int index){
    return GestureDetector(
      onTap: (){
        //Tela de adicionar um cliente a uma mesa
        print("Tocou");
        callNextPage(mesas[index]);
      },
      onLongPress: (){
        deleteMesa(mesas[index]);
      },
      child: Container(
        padding: EdgeInsets.all(0),
        width: 30,
        height: 30,
        child: Card(
          color: mesas[index].ocup ? Colors.grey : Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                mesas[index].name,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20
                ),
              ),
              FutureBuilder<String>(
                future: nameCliente(mesas[index].idCliente ?? 0),
                builder: (context, snapshot){
                  if(snapshot.hasData)
                    return Text(snapshot.data, style: TextStyle(fontSize: 12));
                  return Text("");
                }
              )
//              nameCliente()
            ],
          ),
        ),
      )
    );
  }

  callNextPage(Mesa mesa){
    if(mesa.idCliente != null){
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => PedidoScreen(mesa: mesa)
      ));
    }else{
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => ClienteScreen(mesa)
      ));
    }

  }

  Future<String> nameCliente(int id) async {
    String text = "";
    ClienteDataSource ds = ClienteDataSource();

    Cliente cliente = await ds.getCliente(id);

    if(cliente != null)
      text = cliente.name;

    return text;
  }

  void deleteMesa(Mesa mesa){

    int positionList = (int.parse(mesa.name) - 1);

    setState(() {

      dsMesa.deleteMesa(mesa.id).then((item){
        mesas.removeAt(positionList);
      });

    });
    print("Deletou a mesa ${mesa.id}");
    _getAllMesas();
  }

  Future<void> _createNewMesa(){

    //Cria instância de mesa
    Mesa mesa = Mesa();

    //Adiciona instância ao banco de dados
    dsMesa.saveMesa(mesa);

    //Atualiza a tela
    setState(() {
      _getAllMesas();
    });
  }

  void _getAllMesas() {
    dsMesa.getAllMesas().then((list) {

      setState(() {

        for(int i = 0; i < list.length; i++)
          list[i].name = (i+1).toString();

      mesas = list;

      });
    });
  }
}
