import 'dart:io';
import 'package:restaurante/screens/add_item_sc.dart';
import 'package:restaurante/models/item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurante/datasources/item_ds.dart';

class ItemScreen extends StatefulWidget {

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<ItemScreen> {

  ItemDataSource dsItem = ItemDataSource();

  List<Item> items = List();

  @override
  void initState() {
    super.initState();

    _getAllItems();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Itens'),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () => _escolheTipoRefeicao(context),
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return _itemCard(context, index);
        },
        padding: EdgeInsets.all(10),
      ),
    );
  }

  Widget _itemCard(context, index) {
    return GestureDetector(
      onTap: () => _showOptions(context, index),
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: items[index].img != null
                            ? FileImage(File(items[index].img))
                            : AssetImage("images/default-item.jpg"))),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 170,
                      child: Text(
                        items[index].nome ?? "",
                        style:
                        TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Text("R\$ " + items[index].preco.toString(), style: TextStyle(fontSize: 16)),
//                    Text("Arroz, feijão, salada, batata frita e carne", style: TextStyle(fontSize: 12)),
//                    Text("2 pessoas", style: TextStyle(fontSize: 12, color: Colors.grey)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showOptions(BuildContext context, int index){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return BottomSheet(
              onClosing: (){},
              builder: (context){
                return Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {
                              //Quantidade do produto - IMPLEMENTAR
                              
                              Navigator.pop(context, items[index]);
                              Navigator.pop(context, items[index]);
                            },
                            child: Text("Adicionar ao pedido",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: (){
                              Navigator.pop(context);
//                              _showItemPage(item: items[index]);
                            },
                            child: Text("Editar",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {
                              await dsItem.deleteItem(items[index].id);
                              _getAllItems();
                              Navigator.pop(context);
                            },
                            child: Text("Excluir",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
                    ],
                  ),
                );
              });
        }
    );
  }

  void _escolheTipoRefeicao(BuildContext context){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return BottomSheet(
              onClosing: (){},
              builder: (context){
                return Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {

                              Navigator.pop(context);
                              //TODO Comida add
                              _showItemPage(context);
                            },
                            child: Text("Comida",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: (){
                              Navigator.pop(context);
                              //TODO Bebida add
                              _showItemPage(context);
                            },
                            child: Text("Bebida",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {
                              Navigator.pop(context);
                              //TODO Sobremesa add
                              _showItemPage(context);
                            },
                            child: Text("Sobremesa",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
                    ],
                  ),
                );
              });
        }
    );
  }

  void _showItemPage(context, {Item item}) async {
    final recItem = await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return AddItemScreen(item: item);
    }));
    //Se o contato foi alterado
    if(recItem != null){

      //Se o contato alterado é um contato novo ou um editado (parâmetro)
      if(item != null)
        await dsItem.updateItem(recItem);
      else
        await dsItem.saveItem(recItem);

      _getAllItems();
    }
//    await _escolheTipoRefeicao(context);
  }

  //Atualiza lista de contatos
  void _getAllItems() {
    dsItem.getAllItems().then((list) {
      setState(() {
        items = list;
      });
    });
  }

}