import 'dart:io';
import 'package:restaurante/datasources/item_ds.dart';
import 'package:restaurante/models/item.dart';
import 'package:restaurante/models/mesa.dart';
import 'package:restaurante/models/pedido.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurante/datasources/pedido_ds.dart';
import 'package:restaurante/screens/item_sc.dart';

class PedidoScreen extends StatefulWidget {

  final Mesa mesa;

  PedidoScreen({this.mesa});

  @override
  _PedidoState createState() => _PedidoState();
}

class _PedidoState extends State<PedidoScreen> {

  PedidoDataSource dsPedido = PedidoDataSource();
  Mesa mesa;
  List<Pedido> pedidos = List();

  @override
  void initState() {
    super.initState();

    if(widget.mesa != null){
      mesa = widget.mesa;
    }

    _getPedidosCliente();

//    dsPedido.deletePedido(1);
//
//    Pedido c = Pedido();
//    c.descricao = "José Vinicius";
//    c.email = "josevinicius@gmail.com";
//    c.phone = "987415240";
//    dsPedido.savePedido(c);

//      Pedido c = Pedido();
//      c.descricao = "Thiago Wallass";
//      c.email = "thiagowallass@gmail.com";
//      c.phone = "988984133";
//      dsPedido.savePedido(c);

//    dsPedido.deletePedido(8).then((data) => print('apagou'));

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Pedidos'),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showItemPage(mesa: mesa),
        child: Icon(Icons.add),
        backgroundColor: Colors.red,
      ),
      body: body()
    );
  }

  Widget body(){
    if(pedidos.length == 0){
      return Center(child: Text("Nenhum pedido feito", style: TextStyle(color: Colors.grey)));
    }else{
      return Column(
        children: <Widget>[
          Container(
            height: 450,
            child: ListView.builder(
              itemCount: pedidos.length,
              itemBuilder: (context, index) {
                return _pedidoCard(context, index);
              },
              padding: EdgeInsets.all(10),
            ),
          ),
          RaisedButton(
            color: Colors.red,
            child: Container(child: Text('Total do Pedido', style: TextStyle(color: Colors.white))),
            onPressed: (){}
          )
        ],
      );
    }
  }

  Widget _pedidoCard(context, index) {
    print(pedidos[index]);
    return GestureDetector(
      onTap: () => _showOptions(context, index),
      onLongPress: (){
        dsPedido.deletePedido(pedidos[index].id);
        _getPedidosCliente();
      },
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: FutureBuilder<Map>(
                    future: findItem(pedidos[index].idItem, pedidos[index].id),
                    builder:(context, snapshot){
//                      print(snapshot.data.toString() + " chegou");

                      if(snapshot.hasData){
                        return createRow(snapshot.data);
                      }else
                        return Container(color: Colors.red, height: 30, width: 300,);
                    }
                  ),
                )
              ],
            ),
          ),
          Divider()
        ],
      ),
    );
  }

  Future<Map> findItem(int idItem, int idPedido) async {
    Map responseQuery = Map();
    ItemDataSource dsItem = ItemDataSource();
    PedidoDataSource dsPedido = PedidoDataSource();

//    print(idItem.toString() + " " + idPedido.toString());

    Item item = await dsItem.getItem(idItem);
    responseQuery["descricao"] = item.nome;
    responseQuery["preco"] = item.preco;
    Item aux = item;

//    print(item);

    Pedido pedido = await dsPedido.getPedido(idPedido);
    responseQuery["data"] = pedido.data;
    print(pedido);

//    print(responseQuery.toString() + " enviou");



    return responseQuery;

  }

  Widget createRow(data){
    print(data);
    return Row(
      children: <Widget>[
        Container(
          width: 110,
          child: Text(
            data["descricao"],
            style:
            TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        Text("R\$ " + data["preco"].toString(), style: TextStyle(fontSize: 16)),
        Container(
          margin: EdgeInsets.only(left: 10),
          width: 80,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(data["data"].toString(), style: TextStyle(fontSize: 14)),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(4),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.blue
          ),
          child: Text('Na Fila',
              style: TextStyle(
                  fontSize: 12,
                  color: Colors.white
              )
          ),
        ),
      ],
    );
  }

  void _showOptions(BuildContext context, int index){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return BottomSheet(
              onClosing: (){},
              builder: (context){
                return Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: FlatButton(
                            onPressed: () async {
                              await dsPedido.deletePedido(pedidos[index].id);
                              _getPedidosCliente();
                              Navigator.pop(context);
                            },
                            child: Text("Excluir",
                              style: TextStyle(color: Colors.red, fontSize: 20),
                            )
                        ),
                      ),
                    ],
                  ),
                );
              });
        }
    );
  }

  void _showItemPage({mesa}) async {

    Item item = await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ItemScreen();
    }));

    if(item != null){
      var now = DateTime.now();
      Pedido pedido = Pedido.fromIds(item.id, mesa.idCliente, "${now.day}/${now.month}/${now.year}\n ${now.hour}:${now.minute}");
      await dsPedido.savePedido(pedido);

      _getPedidosCliente();
    }
  }


  void _getPedidosCliente() {
    dsPedido.getPedidosCliente(mesa.idCliente).then((list) {
      setState(() {
        pedidos = list;
      });
    });
  }

}