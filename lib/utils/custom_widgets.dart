import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomWidgets{

  static const IconData sponKnife = IconData(0xe9a3, fontFamily: 'icomoon');

  static Widget appBar({@required String title, Function popFunction, List<Widget> actions, Color backgroundColor}){
    return AppBar(
      title: Text(title),
      automaticallyImplyLeading: false,
      centerTitle: true,
      leading: popFunction != null ?
      IconButton(icon: Icon(Icons.chevron_left), onPressed: popFunction)
          : null,
      actions: actions,
    );
  }

}