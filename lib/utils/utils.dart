

final String clienteTable = "contactTable";
final String itemTable = "itemTable";
final String mesaTable = "mesaTable";
final String pedidoTable = "pedidoTable";
final String itemPedidoTable = "itemPedidoTable";

final String idClienteMesa = "idClienteColumn";
final String ocupColumn = "ocupColumn";

final String nomeColumn = "nome";
final String descricaoColumn = "descricao";
final String precoColumn = "preco";

final String idClientePedido = "idClientePedido";
final String idItemPedido = "idItemPedido";
final String quantidadeColumn = "quantidadeColumn";
final String dataColumn = "dataColumn";

final String idColumn = "idColumn";
final String nameColumn = "nameColumn";
final String emailColumn = "emailColumn";
final String phoneColumn = "phoneColumn";
final String imgColumn = "imgColumn";